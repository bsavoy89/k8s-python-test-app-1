#!flask/bin/python
from flask import Flask
import time

app = Flask(__name__)
@app.route('/')
def index():
    return "TRAGDOOOOOOORRRR!!!"
@app.route('/healthcheck')
def health():
#Play with this sleep to demonstrate health checking
    time.sleep(5) 
    isUp = True   
    return "ok"
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
